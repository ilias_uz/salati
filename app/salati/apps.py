from __future__ import unicode_literals

from django.apps import AppConfig


class SalatiConfig(AppConfig):
    name = 'salati'
