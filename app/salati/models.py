from __future__ import unicode_literals

from django.db import models


class Salati(models.Model):
    name = models.CharField(max_length=200)
    price = models.IntegerField()
    image = models.ImageField(upload_to='salat_img', width_field='img_with', height_field='img_height')
    img_with = models.IntegerField(default=320)
    img_height = models.IntegerField(default=150)
    about = models.TextField()

    def __unicode__(self):
        return self.name