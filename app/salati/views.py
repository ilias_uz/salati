from django.shortcuts import render
from models import Salati
# Create your views here.


def index(request):
    salat = Salati.objects.all().order_by('-id')[:8]
    salati = reversed(salat)
    return render(request, 'index.html', {'salati': salati})


def salat_info(request, id):
    salat = Salati.objects.get(pk=id)
    return render(request, 'salat_info.html', {'salat': salat})